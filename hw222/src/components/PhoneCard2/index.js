import React from 'react'
import Button2 from "../Button2";
import Modal2 from "../Modal2";
import './PhoneCard2.scss'
import cardsLocalStorage from '../../services/cards'
import favoritesLocalStorage from '../../services/favorites'

class PhoneCard2 extends React.Component {
    constructor(props) {
        super(props);
        this.openModal = this.openModal.bind(this)
        this.closeModal = this.closeModal.bind(this)
        this.showStarAndLocalSt = this.showStarAndLocalSt.bind(this)
        this.removeFromBasketStorage = this.removeFromBasketStorage.bind(this)
    }
    state ={
        starColor: false,
        openModal: false,
        card: [],
        favorite: true,
        showHideBtn: true
    }
    openModal () {
        const {vendor_code} = this.props.card
            if (localStorage.getItem('card') === null ) {
                const localCards = [];
                console.log('typeof localCards --- card',typeof localCards)
                localCards.push(vendor_code);
                localStorage.setItem('card', JSON.stringify(localCards));
            }
            else {
                const localCards = JSON.parse(localStorage.getItem('card')).filter(card => card !== vendor_code);
                localCards.push(vendor_code);
                localStorage.setItem('card', JSON.stringify(localCards));
            }
        this.setState({openModal: true})
        this.setState({showHideBtn: false})
    }
    closeModal () {
        this.setState({openModal: false})
    }
    removeFromBasketStorage () {
        const {vendor_code} = this.props.card
        const localBasket = JSON.parse(localStorage.getItem('card')).filter(id => id !== vendor_code);
        localStorage.setItem('card', JSON.stringify(localBasket));
        this.setState({showHideBtn: true})
        if (this.props.deleteBasket) {
            this.props.deleteFromBasket()
        }
    }

    showStarAndLocalSt () {
        const {vendor_code} = this.props.card
        if (this.state.favorite) {
            const localFavorites = JSON.parse(localStorage.getItem('favorite')) || [];
            localFavorites.push(vendor_code);
            localStorage.setItem('favorite', JSON.stringify(localFavorites));
            this.setState({favorite: false});

        } else {
            const localFavorites = JSON.parse(localStorage.getItem('favorite')).filter(id => id !== vendor_code);
            localStorage.setItem('favorite', JSON.stringify(localFavorites));
            this.setState({favorite: true});
            if (this.props.deleteFavorite) {
                this.props.deleteFrom()
            }

        }
    }

    componentDidMount() {
        const {vendor_code} = this.props.card
        if (localStorage.getItem('favorite') !== null) {
            const favoriteInLS = JSON.parse(localStorage.getItem('favorite'));
            if (favoriteInLS.includes(vendor_code)) {
                this.setState({favorite: false})
            }
            else {
                this.setState({favorite: true})
            }
        }
        if (localStorage.getItem('card') !== null) {
            const favoriteInLS = JSON.parse(localStorage.getItem('card'));
            if (favoriteInLS.includes(vendor_code)) {
                this.setState({showHideBtn: false})
            }
            else {
                this.setState({showHideBtn: true})
            }
        }
    }
    render() {
        const {namePhone, price, color, imageUrl,vendor_code} = this.props.card
        return (
                <div>
                    <div className="card" key={vendor_code}>
                        <div className='img-wrapper'>
                            <img src={imageUrl} className="card-img-top" alt="Фото Телефона"/>
                        </div>
                        <div className="card-body">
                            <h5 className="card-title">Model: {namePhone}</h5>
                            <p className="card-text">Price: {price}</p>
                            <p className="card-text-color">Color: {color}</p>
                            <p>
                                {this.state.favorite
                                    ?
                                    <i className={`fas fa-star redStar`}
                                       onClick={this.showStarAndLocalSt}>Добавить в избранное</i>
                                    :
                                    <i className={`fas fa-star blueStar`}
                                       onClick={this.showStarAndLocalSt}>Удалить из избранного</i>
                                }
                            </p>
                            {this.state.showHideBtn ?
                                <Button2
                                    btnClassName='buttonsDesign modalBtn1'
                                    btnText='Add to Basket'
                                    onClick={this.openModal}
                                />
                                :
                                <Button2
                                    btnClassName='buttonsDesign modal-remove-basket'
                                    btnText='Delete from Basket'
                                    onClick={this.removeFromBasketStorage}
                                />
                            }
                            {this.state.openModal &&
                            <Modal2
                                closeModal={this.closeModal}
                                card={this.props.card}
                            />}
                        </div>
                    </div>
                </div>
            );
    }
}

export default PhoneCard2